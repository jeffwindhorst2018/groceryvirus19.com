@extends('layouts.app')

@section('title', 'Post Search')

@section('content')

    <h1>What are you looking for?</h1>

    <div id="search-items">
        <strong>Search for items</strong>
        @include('forms/search-posts')
    <div>

<script>
$(document).ready(function(){
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(findNearbyStores);
        } else {
            x.html("Geolocation is not supported by this browser.");
        }
    }

    function findNearbyStores(position) {
        lat=position.coords.latitude;
        long=position.coords.longitude;
        $('#store-latitude').val(lat);
        $('#store-longitude').val(long);
        $.get('/stores/nearby', {lat:lat, long:long, html:true}, function(data){
            if(data.storeCount == 0) {
                $('#add-store-form-wrapper').removeAttr('hidden');
            } else {
                $('#select-store-wrapper').append(data.stores);
                $('#select-store-wrapper').removeAttr('hidden');
            }
        });
    }
    getLocation();
});
</script>
@endsection
