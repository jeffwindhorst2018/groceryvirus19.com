@extends('layouts.app')

@section('title', 'Search Results')

@section('content')
    <h1>Search Results</h1>

    <div id="search-results">
        <p id="search-header"><strong>Search results</strong></p>
        @if($storePosts->count() <= 0)
            <p>No results found. <a href="/post/search">Return to search</a></p>
        @else
            @foreach($storePosts as $post)
            <ul>
                <li><strong>{{ $post->store()->first()->name }}</strong></li>
                @if(!empty($post->store()->first()->address))
                <li>{{ $post->store()->first()->address }}</li>
                @endif
                @if(!empty($post->store()->first()->intersection))
                <li>{{ $post->store()->first()->intersection }}</li>
                @endif
                <li class="item-rating">{{ $post->storeItem()->first()->name }} - {{ $post->storeItemRating()->first()->rating }}</li>
                <li class="post-created-date">{{ $post->created_at->format('Y-m-d g:i:s a') }}</li>
                @if(!empty($post->image_path))
                <li><img src="/{{ str_replace('public', 'storage', $post->image_path) }}" width="200px"></li>
                @endif
            </ul>
            @endforeach
        @endif
    </div>

<script>
$(document).ready(function(){

});
</script>
@endsection
