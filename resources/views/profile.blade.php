@extends('layouts.app')

@section('title', 'Store Update')

@section('content')
    <h1>Profile</h1>

    <div id="profile-wrapper">
        <div id="profile-favorites-wrapper">
            <h2>Email: {{ Auth::user()->email}}</h2>

            <h3>Favorite Stores</h3>

            {{-- <div id="the-basics">
                <input class="typeahead" type="text" id="search" placeholder="Add favorite store">
            </div> --}}

            <ul id="profile-favorites-list" class="list-unstylized">
                @if($favorites->count() <= 0)
                <li><strong>No favorites set yet.</strong></li>
                @endif
                @foreach($favorites as $favorite)
                <li>
                    <strong>{{ $favorite->name }}</strong>
                @if(!empty($favorite->address))
                    <br>{{ $favorite->address }}
                @endif
                @if(!empty($favorite->intersection))
                    <br>{{ $favorite->intersection }}
                @endif
                </li>
            @endforeach
            </ul>
        </div>

        <div id="profile-custom-query-wrapper">

        </div>
    </div>

<script>
$(document).ready( function () {
    console.log(@json($otherStores));

    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    // var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
    // 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
    // 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
    // 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
    // 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    // 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    // 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
    // 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    // 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    // ];

    // var stores = JSON.parse(@json($otherStores));
    $('#the-basics .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
    {
        name: 'stores',
        // source: substringMatcher(stores)
        source: function (query, process) {
            stores = [];
            map = {};
            var data = JSON.parse(@json($otherStores));
            $.each(data, function(i, store){
                map[store.name] = store;
                stores.push(store.name);
            });
            process(stores);
        },
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                return true;
            }
        },
        sorter: function (items) {
            return items.sort();
        },
        highlighter: function (item) {
            var regex = new RegExp( '(' + this.query + ')', 'gi' );
            return item.replace( regex, "<strong>$1</strong>" );
        },
        updater: function (item) {
            selectedStore = map[item].id;
            console.log('ID ' + item);
            return item;
        }


    });
});
</script>
@endsection

