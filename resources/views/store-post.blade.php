@extends('layouts.app')

@section('title', 'Store Update')

@section('content')

    <h1>New Post</h1>

    @if(Session::has('status'))
    <div class="alert alert-success">
        {{ Session::get('status') }}
    </div>
    @endif

    <div id="store-data">
        <strong>Posting to:</strong>
        <ul>
            <li><strong>Store:</strong> {{ $store->name }}</li>
            @if(!empty($store->address))
            <li><strong>Address:</strong> {{ $store->address }}</li>
            @endif
            @if(!empty($store->intersection))
            <li><strong>Intersection:</strong> {{ $store->intersection }}</li>
            @endif
        </ul>
    <div>
    <div id="add-store-form-wrapper">
        @include('forms.store-post-form')
    </div>

@endsection
