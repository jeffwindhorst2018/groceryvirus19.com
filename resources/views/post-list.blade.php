@extends('layouts.app')

@section('title', 'Store Update')

@section('content')
    <h1>Post List</h1>

    <div id="post-list-wrapper">
        <table id="post-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Store</th>
                    <th>Address Intersection</th>
                    <th>Item</th>
                    <th>Rating</th>
                    <th>Image</th>
                    <th>Comment</th>
                    <th>Posted By</th>
                    <th>Created At</th>
                </tr>
            </thead>
            <tbody>
                @foreach($postList as $post)
                <tr>
                    <td>
                        <strong>{{ $post->store->name }}</strong>
                    </td>
                    <td>
                        {{ $post->store->address ?? '' }}
                        <br>
                        {{ $post->store->intersection ?? '' }}
                    </td>
                    <td>
                        {{ $post->storeItem->name }}
                    </td>
                    <td>
                        {{ $post->storeItemRating->rating }}
                    </td>
                    <td>
                        @if(!empty($post->image_path))
                        <a class="image-link" href="/{{ str_replace('public', 'storage', $post->image_path) }}">
                            <img src="/{{ str_replace('public', 'storage', $post->image_path) }}" width="200px">
                        </a>
                        @endif
                    </td>
                    <td>
                        {{ $post->comment }}
                    </td>
                    <td>
                        @if(!empty($post->user()->first()['name']))
                        {{ $post->user()->first()['name'] }}
                        @endif
                    </td>
                    <td class="text-right">
                        {{ $post->created_at->format('Y/m/d g:i:s a') }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
<script>
$(document).ready( function () {
    $('#post-list').DataTable();
    $('.image-link').magnificPopup({type:'image'});
});
</script>
@endsection
