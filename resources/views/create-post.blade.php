@extends('layouts.app')

@section('title', 'Store Update')

@section('content')
    <h1>Choose a store</h1>

    <div id="select-store-wrapper" hidden>
        <strong>If you don't see your store</strong>
        <button type="button" id="add-store-btn" class="btn btn-primary">Add it!</button>
    </div>
    <div id="add-store-form-wrapper" hidden>
        @include('forms.add_store')
    </div>


<script>
    $(document).ready(function() {
        var x = $("#demo");
        $('body').on('click', '#add-store-btn', function(){
            $('#select-store-wrapper').attr('hidden', 'hidden');
            $('#add-store-form-wrapper').removeAttr('hidden');
        });
        $('body').on('click', '#select-store-wrapper ul li', function(){
            window.location.href = "/store/post/" + $(this).data('store-id');
        });

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(findNearbyStores);
            } else {
                x.html("Geolocation is not supported by this browser.");
            }
        }

        function findNearbyStores(position) {
            lat=position.coords.latitude;
            long=position.coords.longitude;
            $('#store-latitude').val(lat);
            $('#store-longitude').val(long);
            $.get('/stores/nearby', {lat:lat, long:long, html:true}, function(data){
                if(data.storeCount == 0) {
                    $('#add-store-form-wrapper').removeAttr('hidden');

                } else {
                    $('#select-store-wrapper').append(data.stores);
                    $('#select-store-wrapper').removeAttr('hidden');
                }
            });
        }

        $('body').on('click', '#add-store-cancel', function(e){
            e.preventDefault();
            $('#add-store-form-wrapper').attr('hidden', 'hidden');
            $('#select-store-wrapper').removeAttr('hidden');
        });

        $('body').on('click', '#select-store-wrapper ul li a', function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = $(this).children('i');
            sid=$(this).parent('li').data('store-id');
            console.log('Add Favorite ' + sid);
            $.post('/store/add/favorite', {'sid': sid}, function(data){
                if(data.msg == 'added') {
                    console.log("TagName " + $(that).prop('tagName'))
                    $(that).removeClass('far').addClass('fas');
                } else if(data.msg == 'removed') {
                    console.log("TagName " + $(that).prop('tagName'))
                    $(that).removeClass('fas').addClass('far');
                }
            });
        });
        getLocation();
    });
</script>
@endsection
