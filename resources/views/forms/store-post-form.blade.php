<form id="add-store-form" method="post" action="/store/post/save" enctype="multipart/form-data">
    @csrf
    <input id="store-id" type="hidden" name="store_id" value="{{ $store->id }}">
    <div class="form-group">

        <small>If you don&#39;t see the item you can add one here</small>
        <br>
        <input type="text" id="new-item-name" placeholder="New Item">
        <button type="reset" class="btn btn-primary" id="add-item">Add Item</button>
        <div id="new-item-success" class="alert alert-success" role="alert" hidden>
            Thanks for the new item!
        </div>
        <div id="new-item-failure" class="alert alert-danger" role="alert" hidden></div>
        <br><br>

        <label for="store-item-input" class="required">Item: </label>
        <select id="store-item-input" name="store_item" class="form-control" required>
            <option value=""> -- </option>
            @foreach($items as $id => $item)
            <option value="{{ $id }}">{{ $item }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="rating-input" class="required">Rating: </label>
        <select id="rating-input" name="item_rating" class="form-control" required>
            <option value=""> -- </option>
            @foreach($ratings as $id => $rating)
            <option value="{{ $id }}">{{ $rating }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="comment-input">Comments: </label>
        <textarea id="comment-input" name="comment" class="form-control"></textarea>
    </div>
    {{-- Image Upload --}}
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
            </div>
            <div class="custom-file">
                <input type="file" name="post_image" class="custom-file-input" id="inputGroupFile01"
                    aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

<script>
    // Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('#add-item').on('click', function(){
    console.log('Adding new item ' + $('#new-item-name').val());
    $.get('/item/add', {itemName: $('#new-item-name').val()}, function(data){
        console.dir(data);
        if(data.status == 1) {
            $('#new-item-success').removeAttr('hidden');
            $('#new-item-failure').attr('hidden', 'hidden');

            $('#store-item-input').empty();
            $('#store-item-input').append('<option value=""> -- </option>');
            for(i=0; i<data.items.length; i++) {
                $('#store-item-input').append('<option value="'+ data.items[i].id +'">'+ data.items[i].name +'</option>');
            }
        } else if(data.status == 0) {
            $('#new-item-success').attr('hidden', 'hidden');
            $('#new-item-failure').removeAttr('hidden');
            $('#new-item-failure').text(data.msg);
        }
    });
});
</script>
