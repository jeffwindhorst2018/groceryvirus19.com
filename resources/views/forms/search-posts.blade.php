<form id="post-search" method="post" action="/post/search">
    @csrf
    <input id="store-latitude" type="hidden" name="latitude" value="">
    <input id="store-longitude" type="hidden" name="longitude" value="">

    @if($favorites->count() > 0)
    <div class="form-group">
        <input name="store_favorites" type="checkbox" class="" id="store-favorites">
        <label for="store-favorites" class="required">({{ $favorites->count() }}) Favorites only.</label>
    </div>
    @endif

    <div class="form-group" id="store-distance-wrapper">
        <label for="store-distance" class="required">Distance: </label>
        <select id="store-distance" name="store_distance" class="form-control" required>
            <option value=""> -- </option>
            <option value="5">5 Miles</option>
            <option value="10">10 Miles</option>
            <option value="15">15 Miles</option>
            <option value="20">20 Miles</option>
        </select>
    </div>

    <div class="form-group">
        <label for="store-item-input" class="required">Item: </label>
        <select id="store-item-input" name="store_item" class="form-control" required>
            <option value=""> -- </option>
            @foreach($items as $id => $item)
            <option value="{{ $id }}">{{ $item }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<script>
$(document).ready(function(){
    $('#store-favorites').on('change', function(){
        if($(this).prop('checked') == true) {
            $('#store-distance-wrapper').hide();
            $('#store-distance').prop('required', false);
        } else {
            $('#store-distance-wrapper').show();
            $('#store-distance').prop('required', false);
        }
    });
});
</script>
