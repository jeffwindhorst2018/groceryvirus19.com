<p>
    When adding a store, make sure you are <strong>CLOSE</strong> to the store (inside, or in the parking lot).
    Store locations are based upon current GPS coordinates of the person posting.
</P>
<form id="add-store-form" method="post" action="/store/create">
    @csrf
    <input id="store-latitude" type="hidden" name="latitude" value="">
    <input id="store-longitude" type="hidden" name="longitude" value="">
    <div class="form-group">
        <label for="store-name-input">Store Name: </label>
        <input id="store-name-input" type="text" name="store_name" class="form-control" placeholder="Store name" value="">
    </div>
    <div class="form-group">
        <label for="intersection-input">Intersection: </label>
        <input type="text" name="intersection" class="form-control" placeholder="Street1/Street2" value="">
        <small id="intersection-help" class="form-text text-muted">Use the pattern Street1/Street2 for intersections.</small>
    </div>
    <div class="form-group">
        <label for="address-input">Address: </label>
        <input type="text" name="address" class="form-control" placeholder="Address" value="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="reset" id="add-store-cancel" class="btn btn-secondary">Cancel</button>
</form>
