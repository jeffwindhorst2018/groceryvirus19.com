<form id="add-store-form" method="post" action="store/create">
    @csrf
    <div class="form-group">
        <label for="store-select">Choose store</label>
        <select class="selectpicker form-control" id="store-select">
        </select>
      </div>
</form>
