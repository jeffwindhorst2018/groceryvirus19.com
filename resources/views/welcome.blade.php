@extends('layouts.app')

@section('title', 'Store Update')

@section('content')
    <h1>{{ config('app.name') }}</h1>

    <h2>What's new?</h2>
    <p>
        <strong>Register and save favorite stores!</strong> Thanks for using the site! I added the ability to register and log in. So what?
        well, so for now you can at least save your favorite stores! Soon, you can limit searching to only those favorite stores, edit your posts
        if you make a type, and create custom searches for specific items in specific stores.
    </p>
    <p>
        Sorry things are going slowly, teaching children and surviving the quarantine it turns out are demanding jobs!
    </p>

    <p>
        <strong>Stay distant my friends!</strong>
    </p>

    <h2>What is this?</h2>

    <div id="welcome-wrapper">
        <p>
            <strong>Grocery Virus 19</strong> was built by me, Jeff, as a response to numerous neighbors trying to find their necessary groceries during
            the Corona Virus pandemic.
        </p>
        <h3>Posting Instructions</h3>
        <p>
            <ol>
                <li>While you are near the store (inside or in the parking lot), choose "Post" from the navigation at the top.</li>
                <li>Select a nearby store, or add a new store if you don&#39;t find the store you are looking for.</li>
                <li>Add high need items to your post with optional pictures or comments</li>
                <li>Share your post to the site</li>
            </ol>
        </p>
        <h3>Search Instructions</h3>
        <p>
            <small>Search will be more useful once we all add more data (stores and posts) for our local areas.</small>
            <ol>
                <li>Before you leave the house, click "Search" from the navigation at the top.</li>
                <li>Choose the distance you want to search. (This is why it&#39;s so important to be AT the store when you add a new store.)</li>
                <li>Choose the item you are searching for and click "Submit".</li>
                <li>You will see a list of all the posts for the stores near you that mention that item, so that you can better decide where to find your items
                    quickly and easily.</li>
            </ol>
        </p>
        <hr>
        <h3>More about the site</h3>
        <p>
            As 1 person it is impossible for me to find all of your local stores, so this project is &quot;
            <a href="https://medium.com/@ico_snovio/crowdsourcing-data-collection-as-a-new-trend-aa37aa4892a" target="_blank">Crowd Sourced</a>&quot;. Basically
            that means we need all of you to help add stores and create posts so that we all can spend less time in public running out to numerous stores that
            may or may not have what we need.
        </p>
        <p>
            This is my first release and as such, it might be rough! Hell, it may not even work, I don't really know yet! So if you find an issue, let me know.
            jeff@groceryvirus19.com will find me.
        </p>
        <p>
            I plan on making numerous improvements in the coming days and weeks so bear with me, and also, shoot me an email if you have an idea you would
            like to see.
        </p>
        <h3>Pro Tips</h3>
        <ol>
            <li>The total number of stores and posts in the system is displayed in the footer, so you can track the usage along with me!</li>
            <li>When you first use the site from your cellphone, go to the options and choose "Add to home screen". That way you can easily find the site
                whenever you need it.</li>
        </ol>
    </div>

@endsection
