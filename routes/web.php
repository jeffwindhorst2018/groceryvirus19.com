<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@welcome');

// Store routes
Route::post('/store/create', 'StoreController@create');

// Post routes
Route::get('/posts', 'PostController@list');
Route::get('/post/create', 'PostController@createPost');
Route::get('/post/search', 'PostController@search');
Route::post('/post/search', 'PostController@searchRequest');
Route::get('/store/post/{storeId}', 'StoreController@post')->name('store.post');
Route::post('/store/post/save', 'PostController@savePost');

Route::get('/profile', 'UserController@profile')->middleware('auth');

// Ajax routes
Route::get('/stores/nearby', 'AjaxController@storesNearBy');
Route::get('/item/add', 'AjaxController@addItem');
Route::post('/store/add/favorite', 'AjaxController@toggleFavorite');
Route::get('/store/search', 'AjaxController@searchStores');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
