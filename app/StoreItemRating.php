<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreItemRating extends Model
{
    protected $fillable = ['rating'];

    // public function storeItem()
    // {
    //     return $this->belongsTo('App\StoreItem');
    // }
}
