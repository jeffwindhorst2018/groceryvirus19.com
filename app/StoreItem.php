<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreItem extends Model
{
    protected $fillable = ['name'];

    // public function storeItemRating()
    // {
    //     return $this->hasMany('App\StorePost');
    // }

    // public function storePost()
    // {
    //     return $this->belongsTo('App\StorePost');
    // }
}
