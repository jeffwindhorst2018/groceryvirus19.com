<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Store extends Model
{
    protected $fillable = [
        'name',
        'address',
        'intersection',
        'latitude',
        'longitude',
    ];

    public function storePost() {
        return $this->hasMany('App\StorePost');
    }

    public function closeBy($lat, $long, $distance=10) {

        //
        // SQL Function docs:  https://gis.stackexchange.com/questions/31628/find-points-within-a-distance-using-mysql
        //
        $mile = 3959;
        $maxDistance = $distance;
        Log::debug('Distance: ' . $distance);
        Log::debug('Lat: ' . $lat);
        Log::debug('Distance: ' . $long);

        $query = "SELECT
            id,
            name,
            address,
            intersection,
            latitude,
            longitude,
            created_at,
            updated_at,
            (
                " . $mile . " * acos (
                cos ( radians(". $lat .") )
                * cos( radians( latitude ) )
                * cos( radians( longitude ) - radians(". $long .") )
                + sin ( radians(". $lat .") )
                * sin( radians( latitude ) )
                )
            ) AS distance
            FROM stores
            HAVING distance < " . $maxDistance . "
            ORDER BY distance
            LIMIT 0 , 20
        ";
        Log::debug('Query: ' . $query);
        $nearbyStores = DB::select($query);
        // echo 'Near by stores count: ' . count($closeStores);
        return $nearbyStores;
    }
}
