<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class StoreFavorites extends Model
{
    protected $fillable = [
        'user_id',
        'store_id',
    ];

    public function user() {
        return $this->belongsTo(Auth\User);
    }

    public function store() {
        return $this->belongsTo(\App\Store);
    }
}
