<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorePost extends Model
{

    public function storeItem()
    {
        return $this->belongsTo('App\StoreItem');
    }

    public function storeItemRating()
    {
        return $this->belongsTo('App\StoreItemRating');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
