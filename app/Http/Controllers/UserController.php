<?php

namespace App\Http\Controllers;

use App\StoreFavorites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Store;

class UserController extends Controller
{
    public function profile() {
        $favoriteIds = StoreFavorites::where('user_id', Auth::user()->id)->pluck('store_id')->toArray();
        $favorites = Store::whereIn('id', $favoriteIds)->get();
        $otherStores = Store::whereNotIn('id', $favoriteIds)->get(['id', 'name'])->toJson();
        // dd($otherStores);

        return view('profile', compact('favorites', 'otherStores'));
    }
}
