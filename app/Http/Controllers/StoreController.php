<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Store;
use App\StorePost;
use App\StoreItem;
use App\StoreItemRating;
use Illuminate\Support\Str;

class StoreController extends BaseController
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return view('welcome');
    }

    public function create(Request $request) {
        $store = new Store;
        $store->name = $request->store_name;
        $store->address = ( !empty($request->address) ) ? $request->address : null;
        $store->intersection = ( !empty($request->intersection) ) ? $request->intersection : null;
        $store->latitude = $request->latitude;
        $store->longitude = $request->longitude;

        $store->save();

        return redirect('/store/post/'.$store->id);
    }

    public function post($storeId) {
        $store = Store::find($storeId);
        $ratings = StoreItemRating::get()->pluck('rating', 'id')->toArray();
        $items = StoreItem::get()->pluck('name', 'id')->sort()->toArray();
        // dd('STORE ITEMS', $storeItems);
        return view('store-post', compact('store', 'ratings', 'items'));
    }

    public function createPost() {
        dd('in here');
        return view('create-post');
    }

    public function searchPost() {
        dd('Add serach post view here');
    }


}
