<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Store;
use App\StoreFavorites;
use App\StorePost;
use App\StoreItem;
use App\StoreItemRating;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class PostController extends BaseController
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return view('welcome');
    }

    public function list() {
        $postList = StorePost::all()->sortByDesc('created_at');
        // dd($postList->toArray());
        // dd($postList->first()->user()->first()->name);
        return view('post-list', compact('postList'));
    }

    public function create(Request $request) {
        // dd($request->all());
        $store = new Store;
        $store->name = $request->store_name;
        $store->address = ( !empty($request->address) ) ? $request->address : null;
        $store->intersection = ( !empty($request->intersection) ) ? $request->intersection : null;
        $store->latitude = $request->latitude;
        $store->longitude = $request->longitude;

        $store->save();

        return redirect('/store/post/'.$store->id);
    }

    public function post($storeId) {
        $store = Store::find($storeId);
        $ratings = StoreItemRating::get()->pluck('rating', 'id')->toArray();
        $items = StoreItem::get()->pluck('name', 'id')->sort()->toArray();
        // dd('STORE ITEMS', $storeItems);
        return view('store-post', compact('store', 'ratings', 'items'));
    }

    public function createPost() {
        return view('create-post');
    }

    public function search() {
        $items = StoreItem::get()->pluck('name', 'id')->sort()->toArray();

        $favorites = ( Auth::user() ) ? StoreFavorites::where('user_id', Auth::user()->id) : collect();
        return view('post-search', compact('items', 'favorites'));
    }

    public function searchRequest(Request $request) {
        $item = $request->store_item;
        if($request->store_favorites == "on") {
            $storeIds = StoreFavorites::where('user_id', Auth::user()->id)->get()->pluck('store_id')->toArray();
        } else {
            $lat = $request->latitude;
            $long = $request->longitude;
            $distance = $request->store_distance;

            $store = new Store;
            $closeStores = $store->closeBy($lat, $long, $distance);
            $storeIds = [];

            $closeStorePosts = [];
            foreach($closeStores as $closeStore) {
                $storeIds[] = $closeStore->id;
            }
        }
        // TODO Sorting by id is pretty bad, but it's late so I'm just doing it for now.
        $storePosts = StorePost::whereIn('store_id', $storeIds)->where('store_item_id', $item)->get()->sortByDesc('id');

        return view('search-results', ['storePosts' => $storePosts]);
    }

    public function savePost(Request $request) {
        // $status = ( $request->session()->has('status') ) ? $request->session()->get('status') : false;

        $storePost = new StorePost;
        if($request->hasFile('post_image')) {
            $file = $request->post_image;
            $realName = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
            $ext = $file->getClientOriginalExtension();
            $newName = time() . '_' . $realName . '.' . $ext;
            $path = $file->storeAs('public/post_images', $newName, 'local');
            $storePost->image_path = $path;
        }

        if(Auth::check()) {
            $storePost->user_id = auth()->user()->id;
        }
        $storePost->store_id = $request->store_id;
        $storePost->store_item_id = $request->store_item;
        $storePost->store_item_rating_id = $request->item_rating;
        $storePost->comment = $request->comment;
        $storePost->created_at = now();
        $storePost->updated_at = now();
        $storePost->save();

        return back()->with('status', 'Thank you for you post!');
    }
}
