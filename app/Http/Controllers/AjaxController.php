<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\StoreItem;
use App\StoreFavorites;
use Illuminate\Support\Facades\Auth;

class AjaxController extends Controller
{
    public function addItem(Request $request) {
        $item = $request->itemName;
        $checkItem = StoreItem::where('name', $item)->get();
        if($checkItem->count() > 0) {
            return Response(['status' => 0, 'msg' => 'Item already exists.']);
        } else {
            $storeItem = new StoreItem();
            $storeItem->name = $item;
            $storeItem->created_at = now();
            $storeItem->updated_at = now();
            $storeItem->save();

            $allItems = StoreItem::all()->sortBy('name');
            $retItems = [];
            foreach($allItems as $aitem) {
                $retItems[] = [
                    'id' =>$aitem->id,
                    'name' =>$aitem->name,
                ];
            }
            return Response()->json(['status' => '1', 'items' => $retItems]);
        }
    }

    public function storesNearby(Request $request) {
        $lat = $request->lat;
        $long = $request->long;

        $store = new Store();
        $stores = $store->closeBy($lat, $long);

        $response = [
            'lat' => $lat,
            'long' => $long,
            'storeCount' => count($stores),
        ];
        if(!empty($request->html) && $request->html == true) {
            $user = Auth::user();
            $favorites = ( !empty($user) ) ? StoreFavorites::where('user_id', $user->id)->first('store_id') : collect();

            $response['stores'] = "<ul>";
            foreach($stores as $store) {
                $response['stores'] .= "<li data-store-id='" . $store->id . "'>";
                $response['stores'] .= '<strong>' . $store->name . '</strong>';
                $favClass = ( !empty($favorites) && in_array($store->id, $favorites->toArray()) ) ? 'fas' : 'far';
                if(Auth::user()) {
                    $response['stores'] .= '<a href="#">';
                    $response['stores'] .= '<i class="'. $favClass .' fa-heart float-right"></i>';
                    $response['stores'] .= '</a>';
                }
                if(!empty($store->address)) {
                    $response['stores'] .= "<br>" . $store->address;
                }
                if(!empty($store->intersection)) {
                    $response['stores'] .= "<br>" . $store->intersection;
                }
                $response['stores'] .= "</li>";
            }
            return $response;
        } else {
            $response = [

                'stores' => $stores
            ];
            return response()->json($response);
        }
    }

    public function toggleFavorite(Request $request) {
        $user = Auth::user();
        $storeFavorites = new StoreFavorites();
        $storeId = $request->sid;
        $res = [];

        if($favorite = $storeFavorites::where('store_id', $storeId)->where('user_id', $user->id)->first()) {
            StoreFavorites::where('store_id', $storeId)->delete();
            $res['msg'] = 'removed';
        } else {
            $storeFavorites->store_id = $storeId;
            $storeFavorites->user_id = $user->id;
            $storeFavorites->save();
            $res['msg'] = 'added';
        }
        return response()->json($res);
    }

    public function searchStores(Request $request) {
        $favoriteIds = StoreFavorites::where('user_id', Auth::user()->id)->pluck('store_id')->toArray();
        $favorites = Store::whereIn('id', $favoriteIds)->get();
        $otherStores = Store::whereNotIn('id', $favoriteIds)->where('name', 'LIKE', "%{$request->input('query')}%")->get(['id', 'name'])->toJson();
        return response()->json($otherStores);
    }
}
