<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends BaseController
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function welcome(Request $request)
    {
        return view('welcome');
    }

}
