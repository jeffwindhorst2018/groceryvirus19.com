<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\StorePost;

class BaseController extends Controller
{
    public function __construct() {
        $storeCount = Store::all()->count();
        $postCount = StorePost::all()->count();
        // dd($postCount);

        view()->share('storeCount', $storeCount);;
        view()->share('postCount', $postCount);;
    }
}
