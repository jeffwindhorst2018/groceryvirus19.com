<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StorePostsChangeStoreItemRatingsIdName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_posts', function (Blueprint $table) {
            $table->renameColumn('store_item_ratings_id', 'store_item_rating_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_posts', function (Blueprint $table) {
            //
        });
    }
}
