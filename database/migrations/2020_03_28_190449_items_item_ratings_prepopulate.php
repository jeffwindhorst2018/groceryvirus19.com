<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class ItemsItemRatingsPrepopulate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('store_items')->insert(['name' => 'Toilet Paper', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_items')->insert(['name' => 'Meat', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_items')->insert(['name' => 'Seafood', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_items')->insert(['name' => 'Hand Sanitizer', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_items')->insert(['name' => 'Beer', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_items')->insert(['name' => 'Wine', 'created_at' => now(), 'updated_at' => now()]);

        DB::table('store_item_ratings')->insert(['rating' => 'Full', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_item_ratings')->insert(['rating' => 'Fair', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('store_item_ratings')->insert(['rating' => 'Empty', 'created_at' => now(), 'updated_at' => now()]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
