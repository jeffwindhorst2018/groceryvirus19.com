<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StoreFavoritesAddMultiColumnUniqueIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_favorites', function (Blueprint $table) {
            $table->unique(['store_id', 'user_id'], 'store_id_user_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_favorites', function (Blueprint $table) {
            $table->dropIndex('store_id_user_id_index');
        });
    }
}
